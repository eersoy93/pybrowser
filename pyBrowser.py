# Copyright (c) 2023 Erdem Ersoy (eersoy93)
#
# Licensed with MIT license. See LICENSE file for full text.

import PySimpleGUI as psg
import requests

VERSION = "0.0.1"
COPYRIGHT = "Copyright (c) 2023 Erdem Ersoy (eersoy93)"

psg.theme("Topanga")

def fetch_response(url):
    try:
        response = requests.get(url)
        return response.text
    except Exception as e:
        return f"Error fetching the URL! - {e}"

layout = [
    [psg.Text("Enter a URL: "), psg.InputText(key="-URL-"), psg.Button("Fetch")],
    [psg.Multiline(default_text="", size=(65, 20), disabled=True, key="-RESPONSE-")],
]

window = psg.Window(f"pyBrowser {VERSION} - {COPYRIGHT}", layout)

while True:
    event, values = window.read()

    if event == psg.WINDOW_CLOSED:
        break

    if event == "Fetch":
        url = values["-URL-"]
        response = fetch_response(url)
        window["-RESPONSE-"].update(response)

window.close()
