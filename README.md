# pyBrowser
A very simple Web browser with PySimpleGUI. It only views the response text from a URL.

# Author
Erdem Ersoy (eersoy93) (with the help of ChatGPT)

# Copyright and License
Copyright (c) 2023 Erdem Ersoy (eersoy93)

Licensed with MIT license. See LICENSE file for full text.
